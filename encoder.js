$(function() {
    var example = document.getElementById("example"),
            ctx = example.getContext('2d'), // Контекст
            pic = new Image();              // "Создаём" изображение
    pic.src = 'image/test.jpg';  // Источник изображения, позаимствовано на хабре
    pic.onload = function() {    // Событие onLoad, ждём момента пока загрузится изображение
        ctx.drawImage(pic, 0, 0);  // Рисуем изображение от точки с координатами 0, 0

        var x = 0,
                y = 0,
                width = $('#example').width(),
                height = $('#example').height();

        invert(ctx, x, y, width, height);
    };
});

function invert(ctx, x, y, width, height) {
    var imgd = ctx.getImageData(x, y, width, height);
    var pix = imgd.data;


    for (var i = 0, n = pix.length; i < n; i += 4) {
        pix[i  ] = 255 - pix[i  ]; // red
        pix[i + 1] = 255 - pix[i + 1]; // green
        pix[i + 2] = 255 - pix[i + 2]; // blue
        // i+3 is alpha (the fourth element)
    }


    ctx.putImageData(imgd, x, y);
}